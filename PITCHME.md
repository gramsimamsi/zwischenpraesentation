## Zwischenpräsentation

@css[text-pink](@fa[calendar] 28. November, 2019)

@snap[south-west template-note text-gray]
Der aktuelle Stand
@snapend

---

@title[Title + List Fragments]


@snap[north-west]
The Agenda [ Step-by-Step ]
@snapend

@snap[south-west list-content-concise span-100]
@ol
- Lorem ipsum dolor sit amet
- Consectetur adipiscing elit
- Sed do eiusmod tempor
- Ut enim ad minim veniam
- Duis aute irure dolor in
- Excepteur sint occaecat
- Cupidatat non proident
- Sunt in culpa qui officia
@olend
<br><br>
@snapend

@snap[south-west template-note text-gray]
Concise list-item fragments template.
@snapend

---


@snap[north-west]
The Key Concepts [ Step-by-Step ]
@snapend

@snap[west list-content-verbose span-100]
<br>
@ul[list-bullets-circles]
- Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
- Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
- Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
- Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
@ulend
@snapend

@snap[south-west template-note text-gray]
Verbose list-item fragments template.
@snapend

---

@title[Covered Background]

@snap[west text-black span-15]
**@size[1.2em](Where words fail, music speaks.)**
@snapend

@snap[south-west template-note text-white]
Covered background image template.
@snapend


+++?image=template/img/dataflow.png&size=contain
@title[Contained Background]

@snap[south-west template-note text-gray]
Contained background image template.
@snapend

---

## Hola!

---

## Goodbye!

+++

## Adiós!
